Build
=======
`make all`

Tool for automation work with keys

Usage
=======
```
./keyworker -p <port>
./keyworker -h

-p <port>: port to listen on (default 55554)
-d 1: show debug
-w <threads>: run num of threads (0 to 6)
-h: prints this help text
```

API
=======
Upload new key:
```
curl --data-ascii 5D56F45ED57EA8DC9CF62322849A36E8D563AE8C7E5A265CFD994213834B73A4 127.0.0.1:55554
```
Get new key:
```
curl --data-ascii last 127.0.0.1:55554
```
Get key by hash:
```
curl --data-ascii key5D56F45ED57EA8DC9CF62322849A36E8D563AE8C7E5A265CFD994213834B73A4 127.0.0.1:55554
```
